/**
 * FirstServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.hcl;

public class FirstServiceLocator extends org.apache.axis.client.Service implements com.hcl.FirstService {

    public FirstServiceLocator() {
    }


    public FirstServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public FirstServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for First
    private java.lang.String First_address = "http://localhost:8759/WebServiceProject/services/First";

    public java.lang.String getFirstAddress() {
        return First_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String FirstWSDDServiceName = "First";

    public java.lang.String getFirstWSDDServiceName() {
        return FirstWSDDServiceName;
    }

    public void setFirstWSDDServiceName(java.lang.String name) {
        FirstWSDDServiceName = name;
    }

    public com.hcl.First getFirst() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(First_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getFirst(endpoint);
    }

    public com.hcl.First getFirst(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.hcl.FirstSoapBindingStub _stub = new com.hcl.FirstSoapBindingStub(portAddress, this);
            _stub.setPortName(getFirstWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setFirstEndpointAddress(java.lang.String address) {
        First_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.hcl.First.class.isAssignableFrom(serviceEndpointInterface)) {
                com.hcl.FirstSoapBindingStub _stub = new com.hcl.FirstSoapBindingStub(new java.net.URL(First_address), this);
                _stub.setPortName(getFirstWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("First".equals(inputPortName)) {
            return getFirst();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://hcl.com", "FirstService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://hcl.com", "First"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("First".equals(portName)) {
            setFirstEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
