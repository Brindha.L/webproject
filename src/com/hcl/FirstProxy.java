package com.hcl;

import java.rmi.RemoteException;

public class FirstProxy implements com.hcl.First {
  private String _endpoint = null;
  private com.hcl.First first = null;
  
  public FirstProxy() {
    _initFirstProxy();
  }
  
  public FirstProxy(String endpoint) {
    _endpoint = endpoint;
    _initFirstProxy();
  }
  
  private void _initFirstProxy() {
    try {
      first = (new com.hcl.FirstServiceLocator()).getFirst();
      if (first != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)first)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)first)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (first != null)
      ((javax.xml.rpc.Stub)first)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.hcl.First getFirst() {
    if (first == null)
      _initFirstProxy();
    return first;
  }
  
  public java.lang.String hai() throws RemoteException{
    if (first == null)
      _initFirstProxy();
    return first.hai();
  }
  
  public int add(int a, int b) throws RemoteException{
    if (first == null)
      _initFirstProxy();
		return first.add(a, b);
	
  }
  
  
}